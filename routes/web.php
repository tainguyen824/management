<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Botble\Statistic\Models\Commit;
use Botble\Statistic\Models\Contributor;
use Botble\Statistic\Models\Developer;
use Botble\Statistic\Models\Issue;
use Botble\Statistic\Models\Pull;
use Botble\Statistic\Models\Repository;

Route::get("test", function () {
    $data = json_decode(file_get_contents(public_path("docs/json/provinces.json")));
    $prov = array_column((array) $data->VN, "name");
    file_put_contents(public_path("docs/json/provinces.json"), json_encode($prov, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
});
