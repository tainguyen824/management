<?php

namespace Database\Seeders;

use Botble\Candidate\Models\Candidate;
use Faker\Factory;
use Illuminate\Database\Seeder;

class CandidateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $table->string('name');
//        $table->string('code')->unique();
//        $table->text('description')->nullable();
//        $table->date('date_of_birth')->nullable();
//        $table->string('province')->nullable();
//        $table->string('country')->nullable();
//        $table->integer('rank')->default(0);
//        $table->integer('votes')->default(0);
//        $table->string('image')->nullable();
//        $table->string('status', 60)->default('published');
//        $table->timestamps();
        Candidate::truncate();

        $provinces = (array) json_decode(file_get_contents(public_path("docs/json/provinces.json")));
        $faker = Factory::create();
        for ($i = 0; $i < 50; $i++){
            Candidate::create([
                "name" => $faker->name,
                "code" => sprintf("%'.03d\n", $i + 1),
                "description" => $faker->realText,
                "date_of_birth" => $faker->dateTimeBetween("-26 years", "-19 years")->format("Y-m-d"),
                "province" => $provinces[array_rand($provinces)],
                "country" => "Việt Nam",
                "image" => "miss.png",
                "created_at" => now()
            ]);

            echo "Create candidate " . $i . PHP_EOL;
        }
    }
}
