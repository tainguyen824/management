<?php
namespace App\Http\Controllers;


class APIController extends Controller
{

    /**
     * @SWG\Get (
     *     path="/api/v1/candidates",
     *     description="Get candidate list",
     *     tags={"miss"},
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *     ),
     * )
     */
    /**
     * @SWG\Post (
     *     path="/api/v1/vote",
     *     description="Voting",
     *     tags={"miss"},
     *     @SWG\Parameter(
     *         name="wallet",
     *         in="query",
     *         type="string",
     *         description="Wallet",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="top_1",
     *         in="query",
     *         type="number",
     *         description="Top 1 (ID of candidate)",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="top_3",
     *         in="query",
     *         type="array",
     *         items={},
     *         maxItems=3,
     *         description="Top 3 (ID of candidate)",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="top_5",
     *         in="query",
     *         type="array",
     *         items={},
     *         maxItems=5,
     *         description="Top 5 (ID of candidate)",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="top_5_1",
     *         in="query",
     *         type="number",
     *         description="Top 5 + 1 (ID of candidate)",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="top_10",
     *         in="query",
     *         type="array",
     *         items={},
     *         maxItems=10,
     *         description="Top 10 (ID of candidate)",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *     ),
     * )
     */

}
