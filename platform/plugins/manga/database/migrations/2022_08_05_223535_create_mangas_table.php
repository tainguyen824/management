<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMangasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mangas', function (Blueprint $table) {
//            "name", "url", "reading_chapter", "ending_chapter", "image", "like"
            $table->bigIncrements('id');
            $table->string("name");
            $table->string("url")->nullable();
            $table->float("reading_chapter")->default(0);
            $table->string("ending_chapter")->nullable();
            $table->boolean("like")->default(false);
            $table->string("image")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mangas');
    }
}
