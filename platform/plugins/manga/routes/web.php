<?php

Route::group(['namespace' => 'Botble\Manga\Http\Controllers', 'middleware' => ['web', 'core']], function () {

    Route::group(['prefix' => BaseHelper::getAdminPrefix(), 'middleware' => 'auth'], function () {

        Route::group(['prefix' => 'mangas', 'as' => 'manga.'], function () {
            Route::resource('', 'MangaController')->parameters(['' => 'manga']);
            Route::delete('items/destroy', [
                'as'         => 'deletes',
                'uses'       => 'MangaController@deletes',
                'permission' => 'manga.destroy',
            ]);
        });
    });

});
