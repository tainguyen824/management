<?php

namespace Botble\Manga\Tables;

use Illuminate\Support\Facades\Auth;
use BaseHelper;
use Botble\Base\Enums\BaseStatusEnum;
use Botble\Manga\Repositories\Interfaces\MangaInterface;
use Botble\Table\Abstracts\TableAbstract;
use Illuminate\Contracts\Routing\UrlGenerator;
use Yajra\DataTables\DataTables;
use Html;
use RvMedia;

class MangaTable extends TableAbstract
{

    /**
     * @var bool
     */
    protected $hasActions = true;

    /**
     * @var bool
     */
    protected $hasFilter = true;

    /**
     * MangaTable constructor.
     * @param DataTables $table
     * @param UrlGenerator $urlGenerator
     * @param MangaInterface $mangaRepository
     */
    public function __construct(DataTables $table, UrlGenerator $urlGenerator, MangaInterface $mangaRepository)
    {
        parent::__construct($table, $urlGenerator);

        $this->repository = $mangaRepository;

        if (!Auth::user()->hasAnyPermission(['manga.edit', 'manga.destroy'])) {
            $this->hasOperations = false;
            $this->hasActions = false;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function ajax()
    {
        $data = $this->table
            ->eloquent($this->query())
            ->editColumn('name', function ($item) {
                if (!Auth::user()->hasPermission('manga.edit')) {
                    return $item->name;
                }
                return Html::link(route('manga.edit', $item->id), $item->name);
            })
            ->editColumn('url', function ($item) {
                return Html::link($item->url, "Đọc", ["target" => "_blank"]);
            })
            ->editColumn('image', function ($item) {
                $src = ($item->image && file_exists(public_path("storage/" . $item->image))) ? RvMedia::url($item->image) : RvMedia::getDefaultImage();
                return "<img src='$src' width='100%' />";
            })
            ->editColumn('reading_chapter', function ($item) {
                return $item->ending_chapter ? $item->ending_chapter : implode("/", [$item->reading_chapter, $item->release_chapter]);
            })
            ->editColumn('like', function ($item) {
                $color = $item->color;
                if ($item->like)
                    return "<i class='fa fa-heart' style='color: $color; width: 20px'></i>";
                return "<i class='fa fa-times' style='color: $color; width: 20px'></i>";
            })
            ->editColumn('checkbox', function ($item) {
                return $this->getCheckbox($item->id);
            })
            ->editColumn('last_update', function ($item) {
                return BaseHelper::formatDate($item->last_update);
            })
            ->addColumn('operations', function ($item) {
                return $this->getOperations('manga.edit', 'manga.destroy', $item);
            });

        return $this->toJson($data);
    }

    /**
     * {@inheritDoc}
     */
    public function query()
    {
        $query = $this->repository->getModel()
            ->select([
                'id',
                'name',
                'image',
                'reading_chapter',
                'release_chapter',
                'ending_chapter',
                'last_update',
                'url',
                'like',
                'color',
            ]);

        return $this->applyScopes($query);
    }

    /**
     * {@inheritDoc}
     */
    public function columns()
    {
        return [
            'id' => [
                'title' => trans('core/base::tables.id'),
                'width' => '20px',
            ],
            'image' => [
                'title' => 'Hình',
                'class' => 'text-start',
                'width' => '50px',
                'orderable' => false,
                'searchable' => false
            ],
            'name' => [
                'title' => trans('core/base::tables.name'),
                'class' => 'text-start',
            ],
            'like' => [
                'title' => "Trạng thái",
                'class' => 'text-start',
            ],
            'url' => [
                'title' => "Url",
                'class' => 'text-start',
            ],
            'reading_chapter' => [
                'title' => 'Đang đọc/Chap mới nhất',
                'class' => 'text-start',
            ],
            'last_update' => [
                'title' => "Ngày cập nhật gần đây",
                'width' => '100px',
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function buttons()
    {
        return $this->addCreateButton(route('manga.create'), 'manga.create');
    }

    /**
     * {@inheritDoc}
     */
    public function bulkActions(): array
    {
        return $this->addDeleteAction(route('manga.deletes'), 'manga.destroy', parent::bulkActions());
    }

    /**
     * {@inheritDoc}
     */
    public function getBulkChanges(): array
    {
        return [
            'name' => [
                'title' => trans('core/base::tables.name'),
                'type' => 'text',
                'validate' => 'required|max:120',
            ],
            'like' => [
                'title' => "Đã thích",
                'type' => 'customSelect',
                'choices' => [0 => "Không", 1 => "Có"]
            ],
            'color' => [
                'title' => "Màu",
                'type' => 'customSelect',
                'choices' => ["red", "pink", "black", "green"]
            ],
            'last_update' => [
                'title' => "Ngày cập nhật",
                'type' => 'date',
            ],
        ];
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return $this->getBulkChanges();
    }
}
