<?php

namespace Botble\Manga;

use Illuminate\Support\Facades\Schema;
use Botble\PluginManagement\Abstracts\PluginOperationAbstract;

class Plugin extends PluginOperationAbstract
{
    public static function remove()
    {
        Schema::dropIfExists('mangas');
        Schema::dropIfExists('mangas_translations');
    }
}
