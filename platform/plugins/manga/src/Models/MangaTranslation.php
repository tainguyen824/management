<?php

namespace Botble\Manga\Models;

use Botble\Base\Models\BaseModel;

class MangaTranslation extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mangas_translations';

    /**
     * @var array
     */
    protected $fillable = [
        'lang_code',
        'mangas_id',
        'name',
    ];

    /**
     * @var bool
     */
    public $timestamps = false;
}
