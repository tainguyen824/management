<?php

namespace Botble\Manga\Models;

use Botble\Base\Traits\EnumCastable;
use Botble\Base\Enums\BaseStatusEnum;
use Botble\Base\Models\BaseModel;

class Manga extends BaseModel
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mangas';

    /**
     * @var array
     */
    protected $fillable = ["name", "url", "reading_chapter", "release_chapter", "ending_chapter", "image", "like", "color", "last_update"];

}
