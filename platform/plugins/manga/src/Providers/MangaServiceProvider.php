<?php

namespace Botble\Manga\Providers;

use Botble\Manga\Models\Manga;
use Illuminate\Support\ServiceProvider;
use Botble\Manga\Repositories\Caches\MangaCacheDecorator;
use Botble\Manga\Repositories\Eloquent\MangaRepository;
use Botble\Manga\Repositories\Interfaces\MangaInterface;
use Illuminate\Support\Facades\Event;
use Botble\Base\Traits\LoadAndPublishDataTrait;
use Illuminate\Routing\Events\RouteMatched;

class MangaServiceProvider extends ServiceProvider
{
    use LoadAndPublishDataTrait;

    public function register()
    {
        $this->app->bind(MangaInterface::class, function () {
            return new MangaCacheDecorator(new MangaRepository(new Manga));
        });

        $this->setNamespace('plugins/manga')->loadHelpers();
    }

    public function boot()
    {
        $this
            ->loadAndPublishConfigurations(['permissions'])
            ->loadMigrations()
            ->loadAndPublishTranslations()
            ->loadAndPublishViews()
            ->loadRoutes(['web']);

        Event::listen(RouteMatched::class, function () {
            dashboard_menu()->registerItem([
                'id'          => 'cms-plugins-manga',
                'priority'    => 5,
                'parent_id'   => null,
                'name'        => 'plugins/manga::manga.name',
                'icon'        => 'fa fa-swatchbook',
                'url'         => route('manga.index'),
                'permissions' => ['manga.index'],
            ]);
        });
    }
}
