<?php

namespace Botble\Manga\Repositories\Eloquent;

use Botble\Support\Repositories\Eloquent\RepositoriesAbstract;
use Botble\Manga\Repositories\Interfaces\MangaInterface;

class MangaRepository extends RepositoriesAbstract implements MangaInterface
{
}
