<?php

namespace Botble\Manga\Repositories\Caches;

use Botble\Support\Repositories\Caches\CacheAbstractDecorator;
use Botble\Manga\Repositories\Interfaces\MangaInterface;

class MangaCacheDecorator extends CacheAbstractDecorator implements MangaInterface
{

}
