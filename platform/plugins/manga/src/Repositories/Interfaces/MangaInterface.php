<?php

namespace Botble\Manga\Repositories\Interfaces;

use Botble\Support\Repositories\Interfaces\RepositoryInterface;

interface MangaInterface extends RepositoryInterface
{
}
