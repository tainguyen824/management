<?php

namespace Botble\Manga\Forms;

use Botble\Base\Forms\FormAbstract;
use Botble\Base\Enums\BaseStatusEnum;
use Botble\Manga\Http\Requests\MangaRequest;
use Botble\Manga\Models\Manga;

class MangaForm extends FormAbstract
{

    /**
     * {@inheritDoc}
     */
    public function buildForm()
    {
        $this
            ->setupModel(new Manga)
            ->setValidatorClass(MangaRequest::class)
            ->withCustomFields()
            ->add('name', 'text', [
                'label'      => trans('core/base::forms.name'),
                'label_attr' => ['class' => 'control-label required'],
                'attr'       => [
                    'placeholder'  => trans('core/base::forms.name_placeholder'),
                    'data-counter' => 120,
                ],
            ])
            ->add('reading_chapter', 'number', [
                'label'      => "Đang đọc",
                'label_attr' => ['class' => 'control-label required'],
                'attr'       => [
                    'data-counter' => 10,
                ],
            ])
            ->add('release_chapter', 'number', [
                'label'      => "Đã đọc",
                'label_attr' => ['class' => 'control-label required'],
                'attr'       => [
                    'data-counter' => 10,
                ],
            ])
            ->add('ending_chapter', 'onOff', [
                'label'      => "Đã kết thúc",
                'label_attr' => ['class' => 'control-label'],
            ])
            ->add('last_update', 'date', [
                'label'      => "Ngày cập nhật",
                'label_attr' => ['class' => 'control-label'],
            ])
            ->setBreakFieldPoint('status');
    }
}
