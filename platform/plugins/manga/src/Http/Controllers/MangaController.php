<?php

namespace Botble\Manga\Http\Controllers;

use Botble\Base\Events\BeforeEditContentEvent;
use Botble\Manga\Http\Requests\MangaRequest;
use Botble\Manga\Repositories\Interfaces\MangaInterface;
use Botble\Base\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Exception;
use Botble\Manga\Tables\MangaTable;
use Botble\Base\Events\CreatedContentEvent;
use Botble\Base\Events\DeletedContentEvent;
use Botble\Base\Events\UpdatedContentEvent;
use Botble\Base\Http\Responses\BaseHttpResponse;
use Botble\Manga\Forms\MangaForm;
use Botble\Base\Forms\FormBuilder;

class MangaController extends BaseController
{
    /**
     * @var MangaInterface
     */
    protected $mangaRepository;

    /**
     * @param MangaInterface $mangaRepository
     */
    public function __construct(MangaInterface $mangaRepository)
    {
        $this->mangaRepository = $mangaRepository;
    }

    /**
     * @param MangaTable $table
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(MangaTable $table)
    {
        page_title()->setTitle(trans('plugins/manga::manga.name'));

        return $table->renderTable();
    }

    /**
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function create(FormBuilder $formBuilder)
    {
        page_title()->setTitle(trans('plugins/manga::manga.create'));

        return $formBuilder->create(MangaForm::class)->renderForm();
    }

    /**
     * @param MangaRequest $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function store(MangaRequest $request, BaseHttpResponse $response)
    {
        $manga = $this->mangaRepository->createOrUpdate($request->input());

        event(new CreatedContentEvent(MANGA_MODULE_SCREEN_NAME, $request, $manga));

        return $response
            ->setPreviousUrl(route('manga.index'))
            ->setNextUrl(route('manga.edit', $manga->id))
            ->setMessage(trans('core/base::notices.create_success_message'));
    }

    /**
     * @param int $id
     * @param Request $request
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function edit($id, FormBuilder $formBuilder, Request $request)
    {
        $manga = $this->mangaRepository->findOrFail($id);

        $manga->last_update = date("m/d/Y", strtotime($manga->last_update));

        event(new BeforeEditContentEvent($request, $manga));

        page_title()->setTitle(trans('plugins/manga::manga.edit') . ' "' . $manga->name . '"');

        return $formBuilder->create(MangaForm::class, ['model' => $manga])->renderForm();
    }

    /**
     * @param int $id
     * @param MangaRequest $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function update($id, MangaRequest $request, BaseHttpResponse $response)
    {
        $manga = $this->mangaRepository->findOrFail($id);

        $input = $request->input();
        $input["last_update"] = date("Y-m-d", strtotime($input["last_update"]));
        if ($request->has("ending_chapter"))
            $input["ending_chapter"] = $input["release_chapter"];

        $manga->fill($input);

        $manga = $this->mangaRepository->createOrUpdate($manga);

        event(new UpdatedContentEvent(MANGA_MODULE_SCREEN_NAME, $request, $manga));

        return $response
            ->setPreviousUrl(route('manga.index'))
            ->setMessage(trans('core/base::notices.update_success_message'));
    }

    /**
     * @param int $id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function destroy(Request $request, $id, BaseHttpResponse $response)
    {
        try {
            $manga = $this->mangaRepository->findOrFail($id);

            $this->mangaRepository->delete($manga);

            event(new DeletedContentEvent(MANGA_MODULE_SCREEN_NAME, $request, $manga));

            return $response->setMessage(trans('core/base::notices.delete_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws Exception
     */
    public function deletes(Request $request, BaseHttpResponse $response)
    {
        $ids = $request->input('ids');
        if (empty($ids)) {
            return $response
                ->setError()
                ->setMessage(trans('core/base::notices.no_select'));
        }

        foreach ($ids as $id) {
            $manga = $this->mangaRepository->findOrFail($id);
            $this->mangaRepository->delete($manga);
            event(new DeletedContentEvent(MANGA_MODULE_SCREEN_NAME, $request, $manga));
        }

        return $response->setMessage(trans('core/base::notices.delete_success_message'));
    }
}
