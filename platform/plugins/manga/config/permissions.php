<?php

return [
    [
        'name' => 'Mangas',
        'flag' => 'manga.index',
    ],
    [
        'name'        => 'Create',
        'flag'        => 'manga.create',
        'parent_flag' => 'manga.index',
    ],
    [
        'name'        => 'Edit',
        'flag'        => 'manga.edit',
        'parent_flag' => 'manga.index',
    ],
    [
        'name'        => 'Delete',
        'flag'        => 'manga.destroy',
        'parent_flag' => 'manga.index',
    ],
];
